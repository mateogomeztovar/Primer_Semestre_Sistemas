####Interpretador de los nuevos comandos de usuario
import Clases as C
import Funciones as F
import Matrices as M
DV={}#Dicionario de variables.
LF=[]#Lista de Funciones.
DicF={}#Diccionario de funciones.
VC={}#Diccionario de variables creadas.
def interprete():
    #Interpreta todos los comandos del prompt
    Comando=input()
    print(Comando)
    if Comando[0:13]=='Crear sistema':
        S=C.Sistema(int(Comando[14:]))
        DV['Sistema']=(S)
    elif Comando=='Crear matriz':
        NFilas=int(input("Numero de filas "))
        NColumnas=int(input('Numero de columnas '))
        Fase=input('Fase ')
        EAsociada=input('Empresa asociada ')
        Nombre=input('Nombre de la matriz ')
        Tipo=input('Tipo de la matriz ')
        DV[Nombre]=M.CM(NFilas,NColumnas,Fase,EAsociada,Nombre,Tipo)
    elif Comando=='Suministros':
        NFilas=int(input("Numero de filas "))
        NColumnas=int(input('Numero de columnas '))
        Fase=input('Fase ')
        EAsociada=input('Empresa asociada ')
        Nombre=input('Nombre de la matriz ')
        Tipo='Suministros'
        DV[Nombre]=M.Suministros(NFilas,NColumnas,Fase,EAsociada,Nombre,Tipo)
    elif Comando=='Modificar matriz':
        Nombre_Matriz=input('Nombre de la matriz ')
        Tipo_de_la_Empresa=input('Tipo de la empresa ')
        M.Modificar_Matriz(Nombre_Matriz,Tipo_de_la_Empresa)
    elif Comando=='Consultar nombres de matrices':
        print(M.Ma)
    elif Comando[24:]=='Consultar diccionarios de DPMP':
        print (D.DPMP)
    elif Comando[24:]=='Consultar diccionarios de DDP':
        print(D.DDP)
    elif Comando[24:]=='Consultar diccionarios de DF':
        print(D.DF)
    elif Comando[24:]=='Consultar diccionarios de DDM':
        print(D.DDM)
    elif Comando[24:]=='Consultar diccionarios de DDm':
        print(D.DDm)
    elif Comando[24:]=='Consultar diccionarios de DMS':
        print(D.DMS)
    elif Comando[24:]=='Consultar diccionarios de DC':
        print(D.DC)
    elif Comando=='Consultar lista de Productores de Materias Primas':
        print(C.LPMP)
    elif Comando=='Consultar lista de Distribuidores Primarios':
        print(C.LDP)
    elif Comando=='Consultar lista de Fabricas':
        print(C.LF)
    elif Comando=='Consultar lista de Distribuidores Mayoristas':
        print(C.LDM)
    elif Comando=='Consultar lista de Distribuidores minoristas':
        print(C.LDm)
    elif Comando=='Consultar lista de Empresas del sistema':
        print(C.LE)
    elif Comando=='Consultar posibles fases del sistema':
        print(M.Fases)
    elif Comando=='Consultar posibles tipos de matrices del sistema para asignar':
        print(M.Tipos)
    elif Comando=='Calcular utilidades':
        MV=D.DMS[(input('Nombre de la matriz de ventas '))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de costos '))]['Matriz']
        Tipo_de_la_Empresa=input('Tipo de la empresa ')
        Nombre_MU=input('Nombre de la matriz de utilidades ')
        M.Calcular_utilidades(MV,MC,Nombre_MU,Tipo_de_la_Empresa)
    elif Comando=='Crear PMP':
        Nombre=input('Nombre para el PMP ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_PMP(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear DP':
        Nombre=input('Nombre para el DP ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_DP(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear F':
        Nombre=input('Nombre para el F ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_F(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear DM':
        Nombre=input('Nombre para el DM ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_DM(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear Dm':
        Nombre=input('Nombre para el Dm ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_Dm(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Vender Producto':
        Producto=input('Cual es el producto a vender ')
        Empresa_compradora=input('Cual es la empresa compradora ')
        Cantidad=int(input('Cual es la cantidad de producto '))
        Precio_unidad=int(input('Cual es el precio por unidad del producto '))
        Empresa_Productora=input('Cual es el nombre de la empresa productora ')
        (DV[Empresa_Productora]).Vender_Producto(Producto,Empresa_compradora,Cantidad,Precio_unidad)
    elif Comando=='Producir':
        Nombre_de_empresa=input('Cual es el nombre de la empresa que va a producir? ')
        Producto=input('Nombre del producto a producir ')
        Cantidad=int(input('Cantidad que se desea producir del producto '))        
        Elementos_Necesarios=F.Calcular_elementos_necesarios((int(input('Cual es la cantidad de productos necesarios? '))))
        (DV[Nombre_de_empresa]).Producir(Producto,cantidad,Elementos_Necesarios)
    ##### Soporte de variables
    elif Comando=='Var':
        Nombre=eval(input('Cual es el nombre que desea que tenga su variable? '))
        Valor=eval(input('Cual es el valor que desea que tenga su variable? '))
        Nombre=Valor
        VC[str(Nombre)]=Valor
    ##### Soporte de ciclos
    elif Comando=='Repetir':
        Numero_de_veces=int(input('Cuantas veces quiere repetir? '))
        Comandos_a_repetir=input('Escriba los comandos a repetir de tal manera que sean separados por un ; ')
        Lista=Comandos_a_repetir.split(';')
        while Numero_de_veces>0:
            for Comando in Lista:
                Mini_inter(Comando)
            Numero_de_veces-=1
    ##### Manejo de Condicionales.
    elif Comando=='When':
        Condicion=eval(input('Cual es la condicion que se desea evaluar'))
        Comandos_a_evaluar=input('Escriba los comandos a evaluar de tal manera que sean separados por un ; ')
        Lista=Comandos_a_evaluar.split(';')
        if Condicion==True:
            for Comando in Lista:
                Mini_inter(Comando)
    ##### Manejo de funciones y procedimientos.
    elif Comando=='Set':
        Nombre=input('Nombre de la Funcion ')
        Comandos_a_evaluar=input('Escriba los comandos a evaluar de tal manera que sean separados por un ; ')
        Lista=Comandos_a_evaluar.split(';')
        LF.append(Nombre)
        DicF[Nombre]=Lista
    elif Comando=='Call':
        Nombre=input('Nombre de la funcion a llamar ')
        x=DicF[Nombre]
        print(x)
        for Comando in x:
            Mini_inter(Comando)
    elif Comando=='Break':
        exit(0)
    else:
        print ('El comando no existe en Logistic_O')
    interprete()
def Mini_inter(Comando):
    #Se usa para interpretar en ciclos y funciones.
    if Comando[0:13]=='Crear sistema':
        S=C.Sistema(int(Comando[14:]))
        DV['Sistema']=(S)
    elif Comando=='Crear matriz':
        NFilas=int(input("Numero de filas "))
        NColumnas=int(input('Numero de columnas '))
        Fase=input('Fase ')
        EAsociada=input('Empresa asociada ')
        Nombre=input('Nombre de la matriz ')
        Tipo=input('Tipo de la matriz ')
        DV[Nombre]=M.CM(NFilas,NColumnas,Fase,EAsociada,Nombre,Tipo)
    elif Comando=='Suministros':
        NFilas=int(input("Numero de filas "))
        NColumnas=int(input('Numero de columnas '))
        Fase=input('Fase ')
        EAsociada=input('Empresa asociada ')
        Nombre=input('Nombre de la matriz ')
        Tipo='Suministros'
        DV[Nombre]=M.Suministros(NFilas,NColumnas,Fase,EAsociada,Nombre,Tipo)
    elif Comando=='Modificar matriz':
        Nombre_Matriz=input('Nombre de la matriz ')
        Tipo_de_la_Empresa=input('Tipo de la empresa ')
        M.Modificar_Matriz(Nombre_Matriz,Tipo_de_la_Empresa)
    elif Comando=='Consultar nombres de matrices':
        print(M.Ma)
    elif Comando[24:]=='Consultar diccionarios de DPMP':
        print (D.DPMP)
    elif Comando[24:]=='Consultar diccionarios de DDP':
        print(D.DDP)
    elif Comando[24:]=='Consultar diccionarios de DF':
        print(D.DF)
    elif Comando[24:]=='Consultar diccionarios de DDM':
        print(D.DDM)
    elif Comando[24:]=='Consultar diccionarios de DDm':
        print(D.DDm)
    elif Comando[24:]=='Consultar diccionarios de DMS':
        print(D.DMS)
    elif Comando[24:]=='Consultar diccionarios de DC':
        print(D.DC)
    elif Comando=='Consultar lista de Productores de Materias Primas':
        print(C.LPMP)
    elif Comando=='Consultar lista de Distribuidores Primarios':
        print(C.LDP)
    elif Comando=='Consultar lista de Fabricas':
        print(C.LF)
    elif Comando=='Consultar lista de Distribuidores Mayoristas':
        print(C.LDM)
    elif Comando=='Consultar lista de Distribuidores minoristas':
        print(C.LDm)
    elif Comando=='Consultar lista de Empresas del sistema':
        print(C.LE)
    elif Comando=='Consultar posibles fases del sistema':
        print(M.Fases)
    elif Comando=='Consultar posibles tipos de matrices del sistema para asignar':
        print(M.Tipos)
    elif Comando=='Calcular utilidades':
        MV=D.DMS[(input('Nombre de la matriz de ventas '))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de costos '))]['Matriz']
        Tipo_de_la_Empresa=input('Tipo de la empresa ')
        Nombre_MU=input('Nombre de la matriz de utilidades ')
        M.Calcular_utilidades(MV,MC,Nombre_MU,Tipo_de_la_Empresa)
    elif Comando=='Crear PMP':
        Nombre=input('Nombre para el PMP ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_PMP(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear DP':
        Nombre=input('Nombre para el DP ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_DP(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear F':
        Nombre=input('Nombre para el F ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_F(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear DM':
        Nombre=input('Nombre para el DM ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_DM(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Crear Dm':
        Nombre=input('Nombre para el Dm ')
        MPP=D.DMS[(input('Nombre de la matriz de Productos Producidos'))]['Matriz']
        MC=D.DMS[(input('Nombre de la matriz de Costos'))]['Matriz']
        MU=D.DMS[(input('Nombre de la matriz de Utilidades'))]['Matriz']
        MV=D.DMS[(input('Nombre de la matriz de Ventas'))]['Matriz']
        Suministros=D.DMS[(input('Nombre de la matriz de Suministros'))]['Matriz']
        x=Nombre
        Nombre=(DV['Sistema']).Crear_Dm(Nombre,MPP,MC,MU,MV,Suministros)
        DV[x]=Nombre
    elif Comando=='Vender Producto':
        Producto=input('Cual es el producto a vender ')
        Empresa_compradora=input('Cual es la empresa compradora ')
        Cantidad=int(input('Cual es la cantidad de producto '))
        Precio_unidad=int(input('Cual es el precio por unidad del producto '))
        Empresa_Productora=input('Cual es el nombre de la empresa productora ')
        (DV[Empresa_Productora]).Vender_Producto(Producto,Empresa_compradora,Cantidad,Precio_unidad)
    elif Comando=='Producir':
        Nombre_de_empresa=input('Cual es el nombre de la empresa que va a producir? ')
        Producto=input('Nombre del producto a producir ')
        Cantidad=int(input('Cantidad que se desea producir del producto '))        
        Elementos_Necesarios=F.Calcular_elementos_necesarios((int(input('Cual es la cantidad de productos necesarios? '))))
        (DV[Nombre_de_empresa]).Producir(Producto,cantidad,Elementos_Necesarios) 
    elif Comando=='Var':
        Nombre=eval(input('Cual es el nombre que desea que tenga su variable? '))
        Valor=eval(input('Cual es el valor que desea que tenga su variable? '))
        Nombre=Valor
        VC[str(Nombre)]=Valor
    elif Comando=='Repetir':
        Numero_de_veces=int(input('Cuantas veces quiere repetir? '))
        Comandos_a_repetir=input('Escriba los comandos a repetir de tal manera que sean separados por un ; ')
        Lista=Comandos_a_repetir.split(';')
        while Numero_de_veces>0:
            for Comando in Lista:
                Mini_inter(Comando)
            Numero_de_veces-=1
    elif Comando=='When':
        Condicion=eval(input('Cual es la condicion que se desea evaluar'))
        Comandos_a_evaluar=input('Escriba los comandos a evaluar de tal manera que sean separados por un ; ')
        Lista=Comandos_a_evaluar.split(';')
        if Condicion==True:
            for Comando in Lista:
                Mini_inter(Comando)
    elif Comando=='Set':
        Nombre=input('Nombre de la Funcion ')
        Comandos_a_evaluar=input('Escriba los comandos a evaluar de tal manera que sean separados por un ; ')
        Lista=Comandos_a_evaluar.split(';')
        LF.append(Nombre)
        DicF[Nombre]=Lista
    elif Comando=='Call':
        Nombre=input('Nombre de la funcion a llamar ')
        x=DicF[Nombre]
        for Comando in x:
            Mini_inter(Comando)
    elif Comando=='Break':
        exit(0)
    else:
        print ('El comando no existe en Logistic_O')
interprete()
