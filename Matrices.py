### Matrices para Logistic_O
import Clases as C
import Funciones as F
import Diccionarios as D
Constantes=[]
Fases=["PMP","DP","F","DM","Dm"]
Tipos=["MPP","MC","MU","MV","Suministros"]# Matriz Productos Producidos, Matriz de Costos, Matriz de utildades, Matriz de ventas.
Ma=[]### Lista de matrices del sistema
def CM(NFilas:int,NColumnas:int,Fase:str,EAsociada:object,Nombre:str,Tipo:str):
    #### Crea la matriz pedida
    if Fase in Fases and Nombre not in Ma and Tipo in Tipos and Tipo!="Suministros":
        MP=[]#### Matriz Pedida
        for Fila in list(range(NFilas)): #Para cada elemento en la Filas se da una lista vacia nueva
            MP.append([])
            for Columna in list(range(NColumnas)):#Para cada elemento en el numero de Columnas se imprime la lista de 0 correspondiente.
                MP[Fila].append(0)
        Ma.append(Nombre)### agrega la matriz creada a la lista de matrices.
        D.DMS[Nombre]={"Tipo":Tipo,"EAsociada":EAsociada,"Fase":Fase,"Matriz":MP}## Se agrega al diccionario de matrices del sistema las caractristicas asociadas a la matriz creada.
        return MP #### Retorna la matriz creada para su asignacion.
    else:
        print("Error, vuelva a intentarlo, recuerde que los tipos pueden ser ",Tipos,"y las EAsociadas deben de estar creadas en ",C.LE)
def Modificar_Matriz (Nombre_Matriz:str,Tipo_de_la_Empresa:str):
    #### Modifica la matriz dada con nuevos valores
    if D.DMS[Nombre]['Tipo']!="MU":
        for NFila in range(len(D.DMS[Nombre]['Matriz'])):
            for NColumna in range(len((D.DMS[Nombre]['Matriz'])[NFila])):
                (D.DMS[Nombre]['Matriz'])[NFila][NColumna]=(int(input("Ingrese el Valor que quiere tener en la posicion fila ",NFila,", columna ",NColumna)))
        Dic=F.Diccionario_por_Tipo_Empresa(Tipo_de_la_Empresa)
        D.Dic[(D.DMS[Nombre]['EAsociada'])][(D.DMS[Nombre]['Tipo'])]=(D.DMS[Nombre]['Matriz'])
    return D.DMS[Nombre]['Matriz'] ### Retorna la matriz para su asignacion.

def Suministros(NFilas:int,NColumnas:int,Fase:str,EAsociada:object,Nombre:str,Tipo='Suministros'):
    if Nombre not in Ma and Tipo=='Suministros':
        MP=[]#### Matriz Pedida
        for Fila in list(range(NFilas)): #Para cada elemento en la Filas se da una lista vacia nueva
            MP.append([])
            for Columna in list(range(NColumnas)):#Para cada elemento en el numero de Columnas se imprime la lista de 0 correspondiente.
                MP[Fila].append(0)
        Ma.append(Nombre)### agrega la matriz creada a la lista de matrices.
        D.DMS[Nombre]={"Tipo":Tipo,"EAsociada":EAsociada,"Matriz":MP}## Se agrega al diccionario de matrices del sistema las caractristicas asociadas a la matriz creada.
        return MP #### Retorna la matriz creada para su asignacion.
    else:
        print("Error, vuelva a intentarlo, recuerde que los tipos pueden ser ",Tipos,"y las EAsociadas deben de estar creadas en ",C.LE)
def Calcular_utilidades(MV:list,MC:list,Nombre_MU:str,Tipo_de_la_Empresa:str):
    for NFilaU in range(len(D.DMS[Nombre_MU]['Matriz'])):
        for NFilaC in range(len(MC)):
            if MV[NFilaU][0]==MC[NFilaC][0]:
                #Busca el elemento en la matriz de costos.
                if NColumnaU==0:
                #Coloca el nombre del elemento.
                    (D.DMS[Nombre_MU]['Matriz'])[NFilaU][0]=MV[NFilaU][0]
                elif NColumnaU==1:
                #Utilidades por unidad vendida
                    (D.DMS[Nombre_MU]['Matriz'])[NFilaU][1]=(MV[NFilaU][1]-MC[NFilaC][1])
                elif NColumnaU==2:
                #Unidades Vendidas
                    (D.DMS[Nombre_MU]['Matriz'])[NFilaU][2]=MV[NFilaU][2]
                elif NColumnaU==3:
                #Unidades Producidas
                    (D.DMS[Nombre_MU]['Matriz'])[NFilaU][3]=MC[NFilaC][2]
                elif NColumnaU==4:
                #Utilidades totales
                    (D.DMS[Nombre_MU]['Matriz'])[NFilaU][4]=(MV[NFilaU][3]-MC[NFilaC][3])
    Dic=F.Diccionario_por_Tipo_Empresa(Tipo_de_la_Empresa)
    D.Dic[(D.DMS[Nombre_MU]['EAsociada'])]["MU"]=(D.DMS[Nombre_MU]['Matriz'])
    return MU
