import Clases as C
import Matrices as M
import Diccionarios as D
###Funciones
TConstantes=["Oferta","Demanda","Suministro"]
#Se usa en la version beta
def Etapas_del_sistema(ei:int,ef:int)->list:
    #### Retorna las etapas de la linea del producto que tiene el sistema a simular
    ees=[] #### Lista de etapas que tiene el sistema
    for i in list(range(ef+1)):
        if i not in list(range(ei)):
            ees.append(i)
    return ees
def Calcular_Constante (EAsociada:str,Tipo:str):
    # Se usa en la version beta
    if Tipo in TConstantes and EAsociada in C.LE:
        pass
def Asignar_Matrices (MPP:list,MC:list,MU:list,MV:list,Suministros:list):
    #No recuerdo :(
    MPP=MPP
    MC=MC
    MU=MU
    MV=MV
    Suministros=Suministros
def Diccionario_por_Tipo_Empresa(Tipo_de_la_Empresa):
    #Toma el tipo de empresa y devuelve el nombre del diccionario de la misma.
    if Tipo_de_la_empresa=='PMP':
        Dic=DPMP
    elif Tipo_de_la_Empresa=='DP':
        Dic=DDP
    elif Tipo_de_la_Empresa=='F':
        Dic=DF
    elif Tipo_de_la_Empresa=='DM':
        Dic=DDM
    elif Tipo_de_la_Empresa=='Dm':
        Dic=DDm
    return Dic
def Calculuar_elementos_necesarios(Cantidad:int):
    #### Calcula la matriz de elementos necesarios para produccion.
    M=[]
    for NFila in range(Cantidad):
        M.append([0,0])
        M[NFila][0]=input('Nombre de elemento ')
        M[NFila][1]=str(input('Cantidad del producto necesaria '))
    return M
        
