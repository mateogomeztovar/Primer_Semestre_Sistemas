import Matrices as M
import Funciones as Fu
import Diccionarios as D
LPMP=[]#### Lista de Productores de materias primas.
LDP=[] #### Lista de Distribuidores de materias primas (distribuidores primarios).
LF=[] #### Lista de Fabricas.
LDM=[] #### Lista de Distribuidores Mayoristas.
LDm=[] #### Lista de Distribuidores minoristas.
LE=[LPMP,LDP,LF,LDM,LDm] #### Lista de empresas
class Sistema(object):
    ### Crea un sistema a simular nuevo
    def __init__(self,NEtapas:int): ### los __init__ son los metodos que se ejecutan por defecto al crear el objeto de tal clase.
        print("Creaste un nuevo sistema a simular con ",NEtapas," etapas, ahora especifique desde donde arranca el sistema y donde termina")
        EI=eval(input ("En que etapa inicia su sistema a simular? ")) #### EI es la Etapa Inicial del sistema a simular
        EF=EI+(NEtapas-1) #### EF es la Etapa Final del sistema a simular
        EES=(Fu.Etapas_del_sistema(EI,EF))#### Es la lista de etapas del sistema a simular
    def Crear_PMP(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros=None):
        # Crea una empresa perteneciente a la clase PMP
        LPMP.append(Nombre) 
        Nombre=PMP(MPP,MC,MU,MV,Suministros)
        D.DPMP[Nombre]={"MPP":MPP,"MC":MC,"MU":MU,"MV":MV,"Suministros":Suministros}
        return Nombre
    def Crear_DP(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        #Crea una empresa perteneciente a la clase DP.
        LDP.append(Nombre)
        Nombre=DP(MPP,MC,MU,MV,Suministros)
        D.DDP[Nombre]={"MPP":MPP,"MC":MC,"MU":MU,"MV":MV,"Suministros":Suministros}
        return Nombre
    def Crear_F(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        #Crea una empresa perteneciente a la clase F.
        LF.append(Nombre)
        Nombre=F(MPP,MC,MU,MV,Suministros)
        D.DF[Nombre]={"MPP":MPP,"MC":MC,"MU":MU,"MV":MV,"Suministros":Suministros}
        return Nombre
    def Crear_DM(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        #Crea una empresa perteneciente a la clase DM.
        LDM.append(Nombre)
        Nombre=DM(MPP,MC,MU,MV,Suministros)
        D.DDM[Nombre]={"MPP":MPP,"MC":MC,"MU":MU,"MV":MV,"Suministros":Suministros}
        return Nombre
    def Crear_Dm(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        #Crea una empresa perteneciente a la clase Dm.
        LDm.append(Nombre)
        Nombre=Dm(MPP,MC,MU,MV,Suministros)
        D.DDm[Nombre]={"MPP":MPP,"MC":MC,"MU":MU,"MV":MV,"Suministros":Suministros}
        return Nombre
class PMP(Sistema):
    def __init__(self,MPP:list,MC:list,MU:list,MV:list,Suministros=None):
        Fase=1
        Fu.Asignar_Matrices(MPP,MC,MU,MV,Suministros)
    def Vender_Producto(self,Producto:str,Empresa_Compradora:str,Cantidad:int,Precio_unidad:int):
        #Realiza una transaccion entre la empresa Compradora y la empresa PMP que le vende.
        for NFilaP in range(len(D.DPMP[self]['MPP'])):
            if Producto in (D.DPMP[self]['MPP'])[NFila]:
                (D.DPMP[self]['MPP'])[NFilaP][1]-=Cantidad
        D.DPMP[self]['MV'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),Empresa_Compradora])
        D.DDP[Empresa_Compradora]['Suministros'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),self])
        D.DDP[Empresa_Compradora]['MC'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad)])
    def Producir(self,Producto:str,Cantidad:int,Costo_Por_Unidad:int):
        #Produce una cantidad de productos determinada
        D.DPMP[self]['MPP'].append([Producto,Cantidad])
        D.DPMP[self]['MC'].append([Producto,Costo_Por_Unidad,Cantidad,(Precio_unidad*Cantidad)])
class DP(Sistema):
    def __init__(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        Fase=2
        Fu.Asignar_Matrices(MPP,MC,MU,MV,Suministros)
    def Vender_Producto(self,Producto:str,Empresa_Compradora:str,Cantidad:int,Precio_unidad:int):
        #Realiza una transaccion entre la empresa Compradora y la empresa DP que le vende.
        for NFilaP in range(len(D.DDP[self]['MPP'])):
            if Producto in (D.DDP[self]['MPP'])[NFila]:
                (D.DDP[self]['MPP'])[NFilaP][1]-=Cantidad
        D.DDP[self]['MV'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),Empresa_Compradora])
        D.DF[Empresa_Compradora]['Suministros'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),self])
        D.DF[Empresa_Compradora]['MC'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad)])
    def Producir(self,Producto:str,Cantidad:int,Elementos_Necesarios:list):
        #Produce una cantidad de productos determinada
        for PFElemento in range(len(Elementos_Necesarios)):
            for FilaS in range(len(D.DDP[self]['Suministros'])):
                if (D.DDP[self]['Suministros'])[FilaS][0]==Elementos_Necesarios[PFElemento][0]:
                    (D.DDP[self]['Suminstros'])[FilaS][1]-=Elementos_Necesarios[PFElemento][1]
        D.DDP[self]['MPP'].append([Producto,Cantidad])
        D.DDP[self]['MC'].append([Producto,Costo_Por_Unidad,Cantidad,(Precio_unidad*Cantidad)])
class F(Sistema):
    def __init__(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        Fase=3
        Fu.Asignar_Matrices(MPP,MC,MU,MV,Suministros)
    def Vender_Producto(self,Producto:str,Empresa_Compradora:str,Cantidad:int,Precio_unidad:int):
        #Realiza una transaccion entre la empresa Compradora y la empresa F que le vende.
        for NFilaP in range(len(D.DF[self]['MPP'])):
            if Producto in (D.DF[self]['MPP'])[NFila]:
                (D.DF[self]['MPP'])[NFilaP][1]-=Cantidad
        D.DF[self]['MV'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),Empresa_Compradora])
        D.DDM[Empresa_Compradora]['Suministros'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),self])
        D.DDM[Empresa_Compradora]['MC'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad)])
    def Producir(self,Producto:str,Cantidad:int,Elementos_Necesarios:list):
        #Produce una cantidad de productos determinada
        for PFElemento in range(len(Elementos_Necesarios)):
            for FilaS in range(len(D.DF[self]['Suministros'])):
                if (D.DF[self]['Suministros'])[FilaS][0]==Elementos_Necesarios[PFElemento][0]:
                    (D.DF[self]['Suminstros'])[FilaS][1]-=Elementos_Necesarios[PFElemento][1]
        D.DF[self]['MPP'].append([Producto,Cantidad])
        D.DF[self]['MC'].append([Producto,Costo_Por_Unidad,Cantidad,(Precio_unidad*Cantidad)])
class DM(Sistema):
    def __init__(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        Fase=4
        Fu.Asignar_Matrices(MPP,MC,MU,MV,Suministros)
    def Vender_Producto(self,Producto:str,Empresa_Compradora:str,Cantidad:int,Precio_unidad:int):
        #Realiza una transaccion entre la empresa Compradora y la empresa DM que le vende.
        for NFilaP in range(len(D.DDM[self]['MPP'])):
            if Producto in (D.DDM[self]['MPP'])[NFila]:
                (D.DDM[self]['MPP'])[NFilaP][1]-=Cantidad
        D.DDM[self]['MV'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),Empresa_Compradora])
        D.DDm[Empresa_Compradora]['Suministros'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),self])
        D.DDm[Empresa_Compradora]['MC'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad)])
    def Producir(self,Producto:str,Cantidad:int,Elementos_Necesarios:list):
        #Produce una cantidad de productos determinada
        for PFElemento in range(len(Elementos_Necesarios)):
            for FilaS in range(len(D.DDM[self]['Suministros'])):
                if (D.DDM[self]['Suministros'])[FilaS][0]==Elementos_Necesarios[PFElemento][0]:
                    (D.DDM[self]['Suminstros'])[FilaS][1]-=Elementos_Necesarios[PFElemento][1]
        D.DDM[self]['MPP'].append([Producto,Cantidad])
        D.DDM[self]['MC'].append([Producto,Costo_Por_Unidad,Cantidad,(Precio_unidad*Cantidad)])
class Dm(Sistema):
    def __init__(self,Nombre:str,MPP:list,MC:list,MU:list,MV:list,Suministros:list):
        Fase=5
        Fu.Asignar_Matrices(MPP,MC,MU,MV,Suministros)
    def Vender_Producto(self,Producto:str,Cantidad:int,Precio_unidad:int,Empresa_Compradora:None):
        #Realiza una transaccion entre un cliente final y la empresa Dm que le vende.
        for NFilaP in range(len(D.DDm[self]['MPP'])):
            if Producto in (D.DDm[self]['MPP'])[NFila]:
                (D.DDm[self]['MPP'])[NFilaP][1]-=Cantidad
        D.DDm[self]['MV'].append([Producto,Precio_unidad,Cantidad,(Precio_unidad*Cantidad),Empresa_Compradora])
    def Producir(self,Producto:str,Cantidad:int,Elementos_Necesarios:list):
        #Produce una cantidad de productos determinada
        for PFElemento in range(len(Elementos_Necesarios)):
            for FilaS in range(len(D.DDm[self]['Suministros'])):
                if (D.DDm[self]['Suministros'])[FilaS][0]==Elementos_Necesarios[PFElemento][0]:
                    (D.DDm[self]['Suminstros'])[FilaS][1]-=Elementos_Necesarios[PFElemento][1]
        D.DDm[self]['MPP'].append([Producto,Cantidad])
        D.DDm[self]['MC'].append([Producto,Costo_Por_Unidad,Cantidad,(Precio_unidad*Cantidad)])
